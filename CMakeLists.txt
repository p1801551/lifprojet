cmake_minimum_required(VERSION 3.17)
project(lifprojet)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")
set(THREADS_PREFER_PTHREAD_FLAG ON)

link_directories(SFML_LIBRARY_DIR)
include_directories(SFML_INCLUDE_DIR)

find_package(SFML 2.5.1 COMPONENTS system window graphics network audio)

if(SFML_FOUND)
    message(STATUS "SFML_INCLUDE_DIR: ${SFML_INCLUDE_DIR}")
    message(STATUS "SFML_LIBRARIES: ${SFML_LIBRARIES}")
    message(STATUS "SFML_VERSION: ${SFML_VERSION}")
endif()


add_executable(lifprojet main.cpp Map/Map.cpp Map/Map.h Map/Tile.cpp Map/Tile.h Voiture/Voiture.cpp Voiture/Voiture.h Obj/Objectif.cpp Obj/Objectif.h Obj/ListObjectifs.cpp Obj/ListObjectifs.h Bat/StationRecharge.cpp Bat/StationRecharge.h Route/Route.cpp Route/Route.h Map/Decor.cpp Map/Decor.h Route/Intersection.cpp Route/Intersection.h PathFinding/PathFinder.cpp PathFinding/PathFinder.h PathFinding/PathFindingTile.cpp PathFinding/PathFindingTile.h ScreenStates/Screens.cpp ScreenStates/Screens.h ScreenStates/WeatherScreen.cpp ScreenStates/WeatherScreen.h Voiture/Vehicule.cpp Voiture/Vehicule.h Voiture/Camion.cpp Voiture/Camion.h Visualisation/VisualisationChemin.cpp Visualisation/VisualisationChemin.h Divers/Tools.cpp Divers/Tools.h Visualisation/VisualisationEtatVehicule.cpp Visualisation/VisualisationEtatVehicule.h Moniteur/MoniteurChemin.cpp Moniteur/MoniteurChemin.h)




target_link_libraries (lifprojet sfml-graphics sfml-window sfml-system)