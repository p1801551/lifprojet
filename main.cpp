#include <iostream>
#include <SFML/Graphics.hpp>
#include "Map/Map.h"
#include "Voiture/Voiture.h"
#include "ScreenStates/WeatherScreen.h"
#include "ScreenStates/Screens.h"
#include "PathFinding/PathFinder.h"
#include "Visualisation/VisualisationChemin.h"
#include "Visualisation/VisualisationEtatVehicule.h"
#include "Moniteur/MoniteurChemin.h"
#include "Obj/Objectif.h"
#include <cmath>
#include <thread>

using namespace std;

void computeChemin(Voiture& v, int numeroVoiture, MoniteurChemin& moniteurChemin);
vector<int> recupererChemin(MoniteurChemin& chemin, int i, int numeroObjectifs);

int main() {
    srand(static_cast <unsigned> (time(0)));
    sf::RenderWindow window(sf::VideoMode(1856, 896), "Tilemap");
    window.setFramerateLimit(30);
    sf::View viewGlobal = window.getView();

    // define the level with an array of tile indices
    // 1 = Intersection, 2 = Route Horizontale, 3 = Route verticale, 13 = Verdure, [17-27] = Décor, 28 = Stations de recharge
    const int ville[] =
            {
                    1, 2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 1,
                    3, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13,
                    13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3,
                    13, 13, 13, 13, 13, 3,
                    3, 17, 21, 3, 21, 20, 22, 13, 3, 21, 18, 13, 20, 21, 18, 13, 20, 22, 13, 3, 21, 17, 21, 20, 3, 22,
                    19, 21, 17, 22, 20, 21, 22, 17, 21, 3, 21, 20, 22, 13, 3, 21, 18, 13, 20, 21, 18, 13, 20, 22, 13, 3,
                    21, 17, 21, 20, 13, 3,
                    1, 2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 1,
                    3, 23, 21, 3, 21, 26, 22, 13, 3, 22, 24, 13, 26, 13, 24, 21, 26, 21, 13, 3, 21, 23, 21, 26, 3, 22,
                    25, 21, 23, 22, 26, 21, 22, 23, 21, 3, 21, 26, 22, 13, 3, 22, 24, 13, 26, 13, 24, 21, 26, 21, 13, 3,
                    21, 23, 21, 26, 13, 3,
                    3, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13,
                    13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3,
                    13, 13, 13, 13, 13, 3,
                    3, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 28, 27, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13,
                    13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 28, 27, 13, 13, 13, 13, 13, 3,
                    13, 13, 13, 13, 13, 3,
                    3, 13, 13, 3, 13, 13, 13, 13, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 13, 13, 13, 13, 3, 13, 13, 13, 13,
                    13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 13, 13, 13, 13, 13,
                    3,
                    3, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13,
                    13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3,
                    13, 13, 13, 13, 13, 3,
                    3, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13,
                    13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3,
                    13, 13, 13, 13, 13, 3,
                    3, 19, 13, 3, 21, 19, 22, 18, 3, 18, 22, 17, 21, 20, 22, 19, 13, 18, 21, 3, 17, 21, 20, 21, 3, 22,
                    18, 21, 17, 22, 19, 21, 21, 19, 13, 3, 21, 19, 22, 18, 3, 18, 22, 17, 21, 20, 22, 19, 13, 18, 21, 3,
                    17, 21, 20, 21, 13, 3,
                    1, 2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 1,
                    3, 25, 22, 3, 21, 25, 13, 24, 3, 24, 21, 23, 22, 26, 13, 25, 21, 24, 13, 3, 23, 21, 26, 22, 3, 21,
                    25, 21, 23, 22, 25, 21, 21, 25, 22, 3, 21, 25, 13, 24, 3, 24, 21, 23, 22, 26, 13, 25, 21, 24, 13, 3,
                    23, 21, 26, 22, 13, 3,
                    3, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13,
                    13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3,
                    13, 13, 13, 13, 13, 3,
                    3, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13,
                    13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3,
                    13, 13, 13, 13, 13, 3,
                    3, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13,
                    13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3,
                    13, 13, 13, 13, 13, 3,
                    3, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13,
                    13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3,
                    13, 13, 13, 13, 13, 3,
                    3, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13,
                    13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3,
                    13, 13, 13, 13, 13, 3,
                    3, 17, 21, 3, 21, 20, 22, 13, 3, 21, 18, 13, 20, 21, 18, 13, 20, 22, 13, 3, 21, 17, 21, 20, 3, 22,
                    19, 21, 17, 22, 20, 21, 22, 17, 21, 3, 21, 20, 22, 13, 3, 21, 18, 13, 20, 21, 18, 13, 20, 22, 13, 3,
                    21, 17, 21, 20, 13, 3,
                    1, 2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 1,
                    3, 23, 21, 3, 21, 26, 22, 13, 3, 22, 24, 13, 26, 13, 24, 21, 26, 21, 13, 3, 21, 23, 21, 26, 3, 22,
                    25, 21, 23, 22, 26, 21, 22, 23, 21, 3, 21, 26, 22, 13, 3, 22, 24, 13, 26, 13, 24, 21, 26, 21, 13, 3,
                    21, 23, 21, 26, 13, 3,
                    3, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13,
                    13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3,
                    13, 13, 13, 13, 13, 3,
                    3, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 28, 27, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13,
                    13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 28, 27, 13, 13, 13, 13, 13, 3,
                    13, 13, 13, 13, 13, 3,
                    3, 13, 13, 3, 13, 13, 13, 13, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 13, 13, 13, 13, 3, 13, 13, 13, 13,
                    13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 13, 13, 13, 13, 13,
                    3,
                    3, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13,
                    13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3,
                    13, 13, 13, 13, 13, 3,
                    3, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13,
                    13, 13, 13, 13, 13, 13, 13, 13, 13, 3, 13, 13, 13, 13, 3, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 3,
                    13, 13, 13, 13, 13, 3,
                    3, 19, 13, 3, 21, 19, 22, 18, 3, 18, 22, 17, 21, 20, 22, 19, 13, 18, 21, 3, 17, 21, 20, 21, 3, 22,
                    18, 21, 17, 22, 19, 21, 21, 19, 13, 3, 21, 19, 22, 18, 3, 18, 22, 17, 21, 20, 22, 19, 13, 18, 21, 3,
                    17, 21, 20, 21, 13, 3,
                    1, 2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 1, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 1,
            };

    std::vector<Screens*> Screens;
    int screen = 4;
    WeatherScreen weatherScreen;
    Screens.push_back(&weatherScreen);

    while(screen > 3)
    {
        screen = Screens[0]->Run(window);
    }



    Map map;
    if (!map.load("Textures/imageroute.png", sf::Vector2u(32, 32), ville, 58, 28)) {
        std::cout << "Erreur : la map n'a pas pu être chargée";
        return -1;
    }

    // 1 = soleil, 2 = neige, 3 = pluie
    switch (screen) {
        case 1:
            map.setMeteo(1);
            break;
        case 2:
            map.setMeteo(2);
            break;
        case 3:
            map.setMeteo(3);
            break;
        default:
            map.setMeteo(1);
    }

    MoniteurChemin moniteurChemin(&map);

    vector<Vehicule*> vehicules = *(new vector<Vehicule*>());

    Voiture* voiture1 = new Voiture(&moniteurChemin, *(new sf::Vector2i(0, 0)), &map, 25000.0, false);
    voiture1->getObjectifs()->insererObjectif(Objectif(44, 7,5));
    voiture1->getObjectifs()->insererObjectif(Objectif(12, 7,5));
    voiture1->getObjectifs()->insererObjectif(Objectif(53, 11,20));
    voiture1->getObjectifs()->insererObjectif(Objectif(36, 3,10));
    voiture1->getObjectifs()->insererObjectif(Objectif(22, 19,10));
    vehicules.push_back(voiture1);

    Voiture* voiture2 = new Voiture(&moniteurChemin, *(new sf::Vector2i(0, 8)), &map, 25000.0, true);
    voiture2->getObjectifs()->insererObjectif(Objectif(5, 19,70));

    Voiture* voiture3 = new Voiture(&moniteurChemin, *(new sf::Vector2i(10, 19)), &map, 25000.0, true);
    voiture3->getObjectifs()->insererObjectif(Objectif(56, 11,70));

    Voiture* voiture4 = new Voiture(&moniteurChemin, *(new sf::Vector2i(35, 19)), &map, 25000.0, true);
    voiture4->getObjectifs()->insererObjectif(Objectif(57, 3,70));

    vehicules.push_back(voiture2);
    vehicules.push_back(voiture3);
    vehicules.push_back(voiture4);

    VisualisationChemin visualisationChemin(&window, map);
    VisualisationEtatVehicule visualisationEtatVehicule(&window);

    std::vector<std::thread> tabThreadVoitures;

    for(int i = 0; i < vehicules.size(); i++)
    {
        tabThreadVoitures.push_back(std::thread(&Vehicule::demarrer, vehicules[i]));
    }

    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {

                /*for(int i = 0; i < vehicules.size();i++)
                {
                    tabThreadVoitures[i].join();
                }*/
                return -1;
            }
            if (event.type == sf::Event::KeyPressed) {
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) {
                    vehicules[0]->gauche();
                } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
                    vehicules[0]->droite();
                } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z)) {
                    vehicules[0]->haut();
                } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
                    vehicules[0]->bas();
                }
            }
        }
        window.clear();
        window.draw(map);

        for (int i = 0; i < vehicules.size(); i++)
        {
            //if (moniteurChemin.getMultiMapSize() != 0)
            //{
                //std::cout << " La taille de la mutimap est != de 0 et la taille actuelle : " << moniteurChemin.getMultiMapSize() << std::endl;
                /*if(vehicules[i]->objectifsTermines() == true)
                {
                    if(vehicules[i]->numeroMission < vehicules[i]->getObjectifs()->listeObjectifs.size())
                    {
                        vehicules[i]->currentChemin = recupererChemin(moniteurChemin, i, vehicules[i]->numeroMission);
                        vehicules[i]->isCheminDefined = true;
                        vehicules[i]->numeroMission++;
                        std::cout << " MAIN : Pour la voiture :  " << i << " la mission suivante est " << vehicules[i]->numeroMission << std::endl;
                    }
                }
                else
                {*/
                    //printf("MAIN::while : followPath\n");
                    //vehicules[i]->followPath(vehicules[i]->getCurrentChemin());
                    visualisationChemin.draw(vehicules[i]->getCurrentChemin());
                    visualisationEtatVehicule.afficherEtatBatterie(*(vehicules[i]));
                //}
            //}
        }

        /*map.followPath(chemin0, map.getTabVoitures()[0]);
        map.followPath(chemin1, map.getTabVoitures()[1]);

        visualisationChemin.draw(chemin0);
        visualisationChemin.draw(chemin1);*/
        try {
            for(int i = 0; i < vehicules.size(); i++)
            {
                //printf("MAIN::while : draw\n");
                if (vehicules[i]->getId() != -1) {
                    window.draw(*(vehicules[i]));
                }
            }
        }
        catch (std::exception& e) {

        }

        /*visualisationEtatVehicule.afficherEtatBatterie(map.getTabVoitures()[0]);
        visualisationEtatVehicule.afficherEtatBatterie(map.getTabVoitures()[1]);*/

        window.display();
    }

    /*for(int i = 0; i < 1; i++)
    {
        tabThreadVoitures[i].join();
    }*/

    return 0;
}

/*vector<sf::Vector2i> recupererChemin(MoniteurChemin& chemin, Vehicule& vehicule) {
    //std::cout << "===MAIN RECUPERER CHEMIN=====" << std::endl;
    //std::cout << " On essaie de recupérer le chemin numero  " << numeroObjectifs  << " pour la voiture " << i << std::endl;
    return chemin.getChemin(vehicule);
}

void computeChemin(MoniteurChemin& moniteurChemin, Vehicule& vehicule) {
    //vehicule.remplirBatterieABloc();
    std::cout << "====MAIN==== : on compute le chemin" << std::endl;
    moniteurChemin.calculerChemin(vehicule);
}*/