//
// Created by kiraloup on 29/03/2021.
//

#ifndef LIFPROJET_MONITEURCHEMIN_H
#define LIFPROJET_MONITEURCHEMIN_H

#include <mutex>
#include <condition_variable>
#include <map>
#include <vector>

#include "../Map/Map.h"
#include "../PathFinding/PathFinder.h"

class MoniteurChemin {
public:
    MoniteurChemin(Map* map);
    Map* getMap();
    void calculerChemin(int idVehicule, sf::Vector2i positionInitiale, Objectif& objectif);
    std::vector<sf::Vector2i> getChemin(int idVehicule);
    int getMultiMapSize();
    ~MoniteurChemin();
private:
    //un mutex car on à besoin d'une section critique sur la queue.
    std::mutex mutex;
    std::condition_variable attenteCalcul;
    //les consomateurs vont calculer les chemin grace à l'algo de dijkstra puis le thread principale va s'occuper de faire bouger les voitures.
    //A voir mais je passerais le chemin completement calculer que je mettrai dans un tableau, le thread principale dessinera alors en parcourant la liste.
    //j'utilise un pair pour lui indiquer le numéro de la voiture sinon on peut pas savoir à quel chemin correspond qu'elle voiture.
    std::multimap<int, std::vector<sf::Vector2i>> tableauChemin;

    Map* map;
};

#endif //LIFPROJET_MONITEURCHEMIN_H