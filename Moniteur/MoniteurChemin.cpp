//
// Created by kiraloup on 29/03/2021.
//

#include "MoniteurChemin.h"

MoniteurChemin::MoniteurChemin(Map* map) {
    this->map = map;
}

void MoniteurChemin::calculerChemin(int idVehicule, sf::Vector2i positionInitiale, Objectif& objectif) {
    std::unique_lock<std::mutex> loc(mutex);
    PathFinder pathFinder = *(new PathFinder(*map));
    //std::cout << "MoniteurChemin : on calcule les chemins et on l'ajoute à la multimap" << std::endl;
    //int i = 1;
    vector<sf::Vector2i> chemin;

    //for(int i = 0; i < idVehicule.getObjectifs()->listeObjectifs.size();i++)
    //{
        chemin = pathFinder.trouverChemin(positionInitiale, objectif);
        //idVehicule.getObjectifs()->supprimerNiemeObjectif(i);
        tableauChemin.insert(std::make_pair(idVehicule, chemin));
        //std::cout << "MoniteurChemin : Pour la idVehicule " <<  numeroVoiture << " on ajoute l'objectifs " << i << std::endl;
    //}

    //std::cout << "MoniteurChemin : on a calculé " <<  i << " chemins pour le véhicule " << numeroVoiture << std::endl;

    attenteCalcul.notify_all();
}

std::vector<sf::Vector2i> MoniteurChemin::getChemin(int idVehicule) {
    std::unique_lock<std::mutex> loc(mutex);
    //std::cout << "on essaie d'avoir le chemin " << numeroObjectif << " pour la voiture" << idVoiture << std::endl;

    //si aucun chemin n'a été calculer OU qu'on n'a pas encore de chemin pour cette voiture alors
    // on attent que calculer chemin fasse un notify all.
    std::multimap<int, std::vector<sf::Vector2i>>::iterator iterator = tableauChemin.find(idVehicule);

    //(iterator == tableauChemin.end())
    /*if(tableauChemin.end() == iterator)
    {
        std::cout << "l'objectifs "<< numeroObjectif << "n'existe pas " << std::endl;
    }*/
    while((tableauChemin.empty()) || tableauChemin.end() == iterator)
    {
        attenteCalcul.wait(loc);
    }

    std::vector<sf::Vector2i> cheminCalcule = iterator->second;
    //std::cout << " GETCHEMIN : on va renvoyer  le chemin de la voiture " << idVoiture
    //          << " le numéro de chemin est " << iterator->first.first << " et le numero de l'objectif " << iterator->first.second <<std::endl;
    //on supprime le chemin car on en plus besoin pour la suite.
    tableauChemin.erase(iterator);
    return cheminCalcule;
}

Map* MoniteurChemin::getMap() {
    return this->map;
}

int MoniteurChemin::getMultiMapSize() {
    return this->tableauChemin.size();
}

MoniteurChemin::~MoniteurChemin() {
    this->tableauChemin;
}