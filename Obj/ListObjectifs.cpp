//
// Created by kiraloup on 04/02/2021.
//

#include "ListObjectifs.h"
#include "../Divers/Tools.h"
#include <functional>

using namespace std;

ListObjectifs::ListObjectifs(Map* map) {
    initialiserListeObjectifs(map);
}

ListObjectifs::ListObjectifs(Map* map, bool insertionAleatoireDObjectifs) {
    initialiserListeObjectifs(map);
    this->insertionAleatoireDObjectifs = insertionAleatoireDObjectifs;
}

void ListObjectifs::initialiserListeObjectifs(Map* map) {
    this->listeObjectifs = *(new vector<Objectif>());
    this->map = map;
}

bool ListObjectifs::objectifSuivant() {
    if (this->listeObjectifs.empty() == false) {
        this->listeObjectifs.erase(this->listeObjectifs.begin());
        if (Tools::pileOuFace() == true) {
            insererObjectifAleatoire();
        }
        if (this->listeObjectifs.empty() == false) {
            return true;
        }
    }
    return false;
}

bool ListObjectifs::supprimerPremierObjectif() {
    if (this->listeObjectifs.empty() == false) {
        this->listeObjectifs.erase(this->listeObjectifs.begin());
        return true;
    }
    return false;
}

bool ListObjectifs::supprimerNiemeObjectif(int n) {
    if (this->listeObjectifs.empty() == false) {
        this->listeObjectifs.erase(this->listeObjectifs.begin() + n);
        if (this->listeObjectifs.empty() == false) {
            return true;
        }
    }
    return false;
}

void ListObjectifs::insererObjectif(Objectif nouvelObjectif) {
    this->listeObjectifs.push_back(nouvelObjectif);
}

void ListObjectifs::insererObjectifPrioritaire(Objectif nouvelObjectif) {
    this->listeObjectifs.insert(this->listeObjectifs.begin(), nouvelObjectif);
}

void ListObjectifs::insererObjectifAleatoire() {
    int randomCoordX;
    int randomCoordY;
    Tile* currentTile = nullptr;

    do {
        if (currentTile != nullptr) {
            currentTile->unlockModifications();
        }
        randomCoordX = (rand() % (this->map->getLargeur()));
        randomCoordY = (rand() % (this->map->getLongueur()));
        currentTile = this->map->getMap()[randomCoordX][randomCoordY];
        currentTile->lockModifications();
    } while (currentTile->isPractical() == false);
    currentTile->unlockModifications();

    this->insererObjectif(Objectif(randomCoordX, randomCoordY,(rand() % 20 + 30)));
}

Objectif& ListObjectifs::getPremierObjectif() {
    if (this->estVide() == false) {
        return ref(this->listeObjectifs[0]);
    }
}

Objectif& ListObjectifs::getNiemeObjectif(int n) {
    if (this->estVide() == false && n >= 0 && n < this->listeObjectifs.size()) {
        return ref(this->listeObjectifs[n]);
    }
}