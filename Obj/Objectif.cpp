//
// Created by kiraloup on 04/02/2021.
//

#include "Objectif.h"

Objectif::Objectif(int x, int y, int poidsMarchandise) {
    this->x = x;
    this->y = y;
    this->poidsMarchandise = poidsMarchandise;
}

Objectif::Objectif(int x, int y) {
    this->x = x;
    this->y = y;
    this->poidsMarchandise = 0;
}

bool Objectif::estAtteint(int x, int y) {
    return (x == this->x && y == this->y);
}


