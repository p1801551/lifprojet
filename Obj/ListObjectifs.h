//
// Created by kiraloup on 04/02/2021.
//

#ifndef LIFPROJET_LISTOBJECTIFS_H
#define LIFPROJET_LISTOBJECTIFS_H

#include "Objectif.h"
#include "../Map/Tile.h"
#include "../Map/Map.h"
#include <vector>

using namespace std;

class ListObjectifs {
public:
    ListObjectifs(Map* map);
    ListObjectifs(Map* map, bool insertionAleatoireDObjectifs);

    std::vector<Objectif> listeObjectifs;

    bool objectifSuivant();
    bool supprimerPremierObjectif();
    bool supprimerNiemeObjectif(int n);
    void insererObjectif(Objectif nouvelObjectif);
    void insererObjectifPrioritaire(Objectif nouvelObjectif);
    void insererObjectifAleatoire();
    Objectif& getPremierObjectif();
    Objectif& getNiemeObjectif(int n);
    inline bool estVide() { return (this->listeObjectifs.empty() == true); };

private:
    void initialiserListeObjectifs(Map* map);

    Map* map;
    bool insertionAleatoireDObjectifs = false;
};
#endif //LIFPROJET_LISTOBJECTIFS_H