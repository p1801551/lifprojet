//
// Created by kiraloup on 04/02/2021.
//

#ifndef LIFPROJET_OBJECTIF_H
#define LIFPROJET_OBJECTIF_H

#include <SFML/Graphics.hpp>

// La voiture doit parcourir le chemin jsuqu'à un objectif
class Objectif {
public:
    Objectif(int x, int y, int poidsMarchandise);
    Objectif(int x, int y);
    bool estAtteint(int x, int y);

    inline int getX() { return this->x; };
    inline int getY() { return this->y; };
    inline sf::Vector2i getPosition() { return *(new sf::Vector2i(this->x, this->y)); };

    inline void setX(int x) { this->x = x; };
    inline void setY() { this->y = y; };
    inline void setPosition(sf::Vector2i nouvellePosition) { this->x = nouvellePosition.x; this->y = nouvellePosition.y; };

    inline int getPoidsMarchandise() { return this->poidsMarchandise; };
private:
    int x;
    int y;
    int poidsMarchandise;
};

#endif //LIFPROJET_OBJECTIF_H