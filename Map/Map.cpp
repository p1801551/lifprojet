//
// Created by kiraloup on 03/02/2021.
//

#include "Map.h"
#include "Tile.h"
#include "Decor.h"
#include "../Route/Intersection.h"
#include <iostream>

using namespace std;

bool Map::load(const std::string& tilename, sf::Vector2u tileSize, const int* map, uint largeur, uint longueur)
{
    if (!tileset.loadFromFile(tilename))
    {
        std::cout << "Erreur : les textures n'ont pas pu etre chargées" << std::endl;
        return false;
    }

    std::cout << "Les textures ont été chargées" << std::endl;
    this->largeur = largeur; // Nombre de colonnes (axe x)
    this->longueur = longueur; // Nombre de lignes (axe y)

    this->map = *(new vector<vector<Tile*>>());

    // Il y a plusieurs type de primitives mais quad est une prmitive 4 points connectés les uns aux autres.
    vertices.setPrimitiveType(sf::Quads);
    vertices.resize(largeur * longueur * 4);
    tileset.setSmooth(true);

    for (int x = 0; x < this->largeur; x++) {
        this->map.push_back(*(new vector<Tile*>()));
        for (int y = 0; y < this->longueur; y++) {
            int indTile = x + y * this->largeur;
            // On obtient le numéro de la case du tableau

            int typeTile = map[indTile];

            Tile *newTile;
            switch (typeTile)
            {
                case 1:
                    newTile = new Intersection();
                    break;
                case 2:
                    newTile = new Route(Tile::TypeTile::RouteHorizontale);
                    break;
                case 3:
                    newTile = new Route(Tile::TypeTile::RouteVerticale);
                    break;
                case 28:
                    newTile = new StationRecharge();
                    vectStationR.push_back(*(new sf::Vector2i(x,y + 1)));
                    break;
                default:
                    newTile = new Decor();
                    break;
            }

            this->map[x].push_back(newTile);

            // On va chercher sa position dans la newTile texture
            int tu = typeTile % (tileset.getSize().x / tileSize.x);
            int tv = typeTile / (tileset.getSize().x / tileSize.x);

            // Un pointeur vers la primitive quad selectionner
            sf::Vertex* quad = &vertices[(x + y * largeur) * 4];

            // On définit ses 4 coins
            quad[0].position = sf::Vector2f(x * tileSize.x, y * tileSize.y);
            quad[1].position = sf::Vector2f((x + 1) * tileSize.x, y * tileSize.y);
            quad[2].position = sf::Vector2f((x + 1) * tileSize.x, (y + 1) * tileSize.y);
            quad[3].position = sf::Vector2f(x * tileSize.x, (y + 1) * tileSize.y);

            // On définit les coordonnées
            quad[0].texCoords = sf::Vector2f(tu * tileSize.x, tv * tileSize.y);
            quad[1].texCoords = sf::Vector2f((tu + 1) * tileSize.x, tv * tileSize.y);
            quad[2].texCoords = sf::Vector2f((tu + 1) * tileSize.x, (tv + 1) * tileSize.y);
            quad[3].texCoords = sf::Vector2f(tu * tileSize.x, (tv + 1) * tileSize.y);
        }
    }
    return true;
}

sf::Vector2i Map::north(int x, int y) {
    int newY = y - 1;
    if (newY < 0) {
        return *(new sf::Vector2i(-1, -1));
    }
    else {
        return *(new sf::Vector2i(x, newY));
    }
}

sf::Vector2i Map::north(sf::Vector2i positionInitiale) {
    return north(positionInitiale.x, positionInitiale.y);
}

sf::Vector2i Map::south(int x, int y) {
    int newY = y + 1;
    if (newY >= this->longueur) {
        return *(new sf::Vector2i(-1, -1));
    }
    else {
        return *(new sf::Vector2i(x, newY));
    }
}

sf::Vector2i Map::south(sf::Vector2i positionInitiale) {
    return south(positionInitiale.x, positionInitiale.y);
}

sf::Vector2i Map::east(int x, int y) {
    int newX = x + 1;
    if (newX >= this->largeur) {
        return *(new sf::Vector2i(-1, -1));
    }
    else {
        return *(new sf::Vector2i(newX, y));
    }
}

sf::Vector2i Map::east(sf::Vector2i positionInitiale) {
    return east(positionInitiale.x, positionInitiale.y);
}

sf::Vector2i Map::west(int x, int y) {
    int newX = x - 1;
    if (newX < 0) {
        return *(new sf::Vector2i(-1, -1));
    }
    else {
        return *(new sf::Vector2i(newX, y));
    }
}

sf::Vector2i Map::west(sf::Vector2i positionInitiale) {
    return west(positionInitiale.x, positionInitiale.y);
}

queue<sf::Vector2i> Map::getCheminsVoisins(sf::Vector2i positionInitiale) {
    queue<sf::Vector2i> voisins;
    //vector<int> voisinsNonTries;

    if (north(positionInitiale).x != -1) {
        Tile* voisinNord = this->map[north(positionInitiale).x][north(positionInitiale).y];

        voisinNord->lockModifications();
        if (north(positionInitiale).x != -1 && voisinNord->isPractical() == true) {
            voisins.push(north(positionInitiale));
        }
        voisinNord->unlockModifications();
    }
    if (south(positionInitiale).x != -1) {
        Tile* voisinSud = this->map[south(positionInitiale).x][south(positionInitiale).y];

        voisinSud->lockModifications();
        if (south(positionInitiale).x != -1 && voisinSud->isPractical() == true) {
            voisins.push(south(positionInitiale));
        }
        voisinSud->unlockModifications();
    }
    if (east(positionInitiale).x != -1) {
        Tile* voisinEst = this->map[east(positionInitiale).x][east(positionInitiale).y];

        voisinEst->lockModifications();
        if (east(positionInitiale).x != -1 && voisinEst->isPractical() == true) {
            voisins.push(east(positionInitiale));
        }
        voisinEst->unlockModifications();
    }
    if (west(positionInitiale).x != -1) {
        Tile* voisinOuest = this->map[west(positionInitiale).x][west(positionInitiale).y];

        voisinOuest->lockModifications();
        if (west(positionInitiale).x != -1 && voisinOuest->isPractical() == true) {
            voisins.push(west(positionInitiale));
        }
        voisinOuest->unlockModifications();
    }

    /*int indiceMinimum = 0;
    while (voisinsNonTries.empty() == false) {
        for (int i = 0; i < voisinsNonTries.size(); i++)  {
            if (((Route*)this->map[voisinsNonTries[i]])->getConsommationElectrique() < ((Route*)this->map[voisinsNonTries[indiceMinimum]])->getConsommationElectrique()) {
                indiceMinimum = i;
            }
        }
        voisins.push(voisinsNonTries[indiceMinimum]);
        indiceMinimum = 0;
        voisinsNonTries.erase(voisinsNonTries.begin());
    }*/

    return voisins;
}

queue<sf::Vector2i> Map::getBornesVoisines(sf::Vector2i positionInitiale) {
    queue<sf::Vector2i> bornesVoisines;
    if (north(positionInitiale).x != -1 && map[north(positionInitiale).x][north(positionInitiale).y]->getTypeTile() == Tile::TypeTile::StationRecharge) {
        bornesVoisines.push(north(positionInitiale));
    }
    if (south(positionInitiale).x != -1 && map[south(positionInitiale).x][south(positionInitiale).y]->getTypeTile() == Tile::TypeTile::StationRecharge) {
        bornesVoisines.push(south(positionInitiale));
    }
    if (east(positionInitiale).x != -1 && map[east(positionInitiale).x][east(positionInitiale).y]->getTypeTile() == Tile::TypeTile::StationRecharge) {
        bornesVoisines.push(east(positionInitiale));
    }
    if (west(positionInitiale).x != -1 && map[west(positionInitiale).x][west(positionInitiale).y]->getTypeTile() == Tile::TypeTile::StationRecharge) {
        bornesVoisines.push(west(positionInitiale));
    }
    return bornesVoisines;
}

vector<sf::Vector2i> Map::getBornesAtteignablesLesPlusProches(sf::Vector2i positionInitiale, double batterieInitiale, double consommationEnergie) {
    vector<sf::Vector2i> stationsRecharge = vectStationR;
    vector<double> coutsStationRecharge(stationsRecharge.size(), 0);
    vector<sf::Vector2i> stationsLesPlusProches;
    PathFinder pathFinder(*this);
    vector<sf::Vector2i> currentChemin;
    for (int i = 0; i < stationsRecharge.size(); i++) {
        currentChemin = pathFinder.trouverChemin(positionInitiale, *(new Objectif(stationsRecharge[i].x, stationsRecharge[i].y)));
        for (int j = 0; j < currentChemin.size(); j++) {
            coutsStationRecharge[i] += ((Route*)map[currentChemin[j].x][currentChemin[j].y])->getConsommationElectrique() + consommationEnergie;
        }
        if (coutsStationRecharge[i] > batterieInitiale) {
            stationsRecharge.erase(stationsRecharge.begin() + i);
            coutsStationRecharge.erase(coutsStationRecharge.begin() + i);
        }
    }

    double coutMin;
    int indMin;
    while (stationsRecharge.size() > 0) {
        coutMin = coutsStationRecharge[0];
        indMin = 0;
        for (int ind = 1; ind < stationsRecharge.size(); ind++) {
            if (coutsStationRecharge[ind] < coutMin) {
                coutMin = coutsStationRecharge[ind];
                indMin = ind;
            }
        }
        stationsLesPlusProches.push_back(stationsRecharge[indMin]);
        stationsRecharge.erase(stationsRecharge.begin() + indMin);
        coutsStationRecharge.erase(coutsStationRecharge.begin() + indMin);
    }

    return stationsLesPlusProches;
}

int Map::getLargeur() {
    return this->largeur;
}

int Map::getLongueur() {
    return this->longueur;
}

void Map::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
    // apply the transform
    states.transform *= getTransform();

    // apply the tileset texture
    states.texture = &tileset;

    // draw the vertex array
    target.draw(vertices, states);
}

void Map::setMeteo(int i) {
    this->meteo = i;
}

int Map::getMeteo() {
    return this->meteo;
}
