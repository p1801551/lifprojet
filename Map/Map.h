//
// Created by kiraloup on 03/02/2021.
//

#ifndef LIFPROJET_MAP_H
#define LIFPROJET_MAP_H

#include <SFML/Graphics.hpp>
#include "Tile.h"
#include "../ScreenStates/Screens.h"
#include <queue>
#include "../Bat/StationRecharge.h"
using namespace std;

class Map : public sf::Drawable, public sf::Transformable {
public:
    bool load(const std::string&, sf::Vector2u, const int*, uint, uint);

    sf::Vector2i north(int x, int y);
    sf::Vector2i north(sf::Vector2i positionInitiale);
    sf::Vector2i south(int x, int y);
    sf::Vector2i south(sf::Vector2i positionInitiale);
    sf::Vector2i east(int x, int y);
    sf::Vector2i east(sf::Vector2i positionInitiale);
    sf::Vector2i west(int x, int y);
    sf::Vector2i west(sf::Vector2i positionInitiale);

    queue<sf::Vector2i> getCheminsVoisins(sf::Vector2i positionInitiale);
    queue<sf::Vector2i> getBornesVoisines(sf::Vector2i positionInitiale);
    vector<sf::Vector2i> getBornesAtteignablesLesPlusProches(sf::Vector2i positionInitiale, double batterieInitiale, double consommationEnergie);

    int getLargeur();
    int getLongueur();
    vector<sf::Vector2i> vectStationR;
    vector<vector<Tile*>> getMap() { return this->map; };

    void setMeteo(int i);
    int getMeteo();

private:
    virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
    vector<vector<Tile*>> map;
    sf::Texture tileset;
    sf::VertexArray vertices;
    int largeur;
    int longueur;
    int meteo;
};

#endif //LIFPROJET_MAP_H