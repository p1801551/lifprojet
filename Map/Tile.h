//
// Created by kiraloup on 03/02/2021.
//

#ifndef LIFPROJET_TILE_H
#define LIFPROJET_TILE_H

#include <SFML/Graphics.hpp>
#include <mutex>
#include <condition_variable>

class Tile {

public:
    enum class TypeTile { Decor, StationRecharge, RouteHorizontale, RouteVerticale, RouteIntersection };
    explicit Tile(TypeTile typeTile);
    TypeTile getTypeTile();
    void setTypeTile(TypeTile newTypeTile);
    void setPractical(bool nouvelleValeur);
    bool isPractical();

    inline void lockModifications() { this->mutex.lock(); };
    inline void unlockModifications() { this->mutex.unlock(); };

    void lockWith(Tile* secondeTileAVerrouiller);
    void unlockWith(Tile* secondeTileADeverrouiller);

protected:
    TypeTile typeTile;
    bool practical;

    std::mutex mutex;
    static std::mutex m_lockWith;
};

#endif //LIFPROJET_TILE_H