//
// Created by kiraloup on 03/02/2021.
//

#include "Tile.h"

std::mutex Tile::m_lockWith;

Tile::Tile(TypeTile typeTile) {
    this->typeTile = typeTile;
}

void Tile::setTypeTile(TypeTile newTypeTile) {
    this->typeTile=newTypeTile;
}

Tile::TypeTile Tile::getTypeTile() {
    return this->typeTile;
}

bool Tile::isPractical() {
    return this->practical;
}

void Tile::setPractical(bool nouvelleValeur) {
    this->practical = nouvelleValeur;
}

void Tile::lockWith(Tile* secondeTileAVerrouiller) {
    m_lockWith.lock();
    this->lockModifications();
    secondeTileAVerrouiller->lockModifications();
}

void Tile::unlockWith(Tile* secondeTileADeverrouiller) {
    m_lockWith.unlock();
    this->unlockModifications();
    secondeTileADeverrouiller->unlockModifications();
}