//
// Created by nico on 09/02/2021.
//

#include <iostream>
#include "Route.h"

Route::Route(TypeTile typeTile) : Tile(typeTile) {
    this->practical = true;
    this->distance = 1;
    this->inclinaison = rand() % 60 + 40;
    this->etat = rand() % 60 + 40;
    this->vehiculePresent = nullptr;
}

int Route::getEtat(){
    return this->etat;
}

int Route::getInclinaison(){
    return this->inclinaison;
}

double Route::getConsommationElectrique() {
    return (this->getEtat() + this->getInclinaison()) / 10;
}

Vehicule* Route::getVehiculePresent() {
    return this->vehiculePresent;
}

void Route::setVehiculePresent(Vehicule* nouveauVehicule) {
    this->vehiculePresent = nouveauVehicule;
};

double Route::getDistance() {
    return this->distance;
}