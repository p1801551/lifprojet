//
// Created by nico on 22/02/2021.
//

#ifndef LIFPROJET_INTERSECTION_H
#define LIFPROJET_INTERSECTION_H
#include "Route.h"

class Intersection : public Route {
public:
    explicit Intersection();
    int calculNbIntersections();
    ~Intersection();
private:
    int nbIntersections;
    bool haut;
    bool bas;
    bool gauche;
    bool droite;
};

#endif //LIFPROJET_INTERSECTION_H