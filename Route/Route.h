//
// Created by nico on 09/02/2021.
//

#ifndef LIFPROJET_ROUTE_H
#define LIFPROJET_ROUTE_H

#include "../Map/Tile.h"
#include "../Voiture/Vehicule.h"
#include <iostream>
#include <SFML/Graphics.hpp>

class Route : public Tile {
    public:
        Route(TypeTile typeTile);
        int getEtat();
        int getInclinaison();
        double getDistance();
        double getConsommationElectrique();
        Vehicule* getVehiculePresent();
        void setVehiculePresent(Vehicule* nouveauVehicule);

    private:
        double distance; // en mètres
        int etat; // de 0 à 100
        int inclinaison; // de 0 à 100
        Vehicule* vehiculePresent;
};

#endif //LIFPROJET_ROUTE_H