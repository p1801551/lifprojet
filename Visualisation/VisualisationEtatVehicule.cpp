//
// Created by minejona on 23/03/2021.
//

#include <sstream>
#include "VisualisationEtatVehicule.h"

VisualisationEtatVehicule::VisualisationEtatVehicule(sf::RenderWindow* window) {
    this->window = window;
    if (!this->font.loadFromFile("FreeSerif.ttf")) {
        cout << "Erreur : le fichier FreeSerif.ttf n'a pas pu être chargé" << endl;
    }
}

void VisualisationEtatVehicule::afficherEtatBatterie(Vehicule& vehicule) {
    ostringstream strs;
    strs << vehicule.getBatterie();
    string batterie = strs.str();

    sf::Text text;
    text.setFont(this->font);
    text.setOrigin(16.0,32.0);
    text.setString(batterie);
    text.setCharacterSize(32);
    text.setFillColor(sf::Color(255, 255 * (vehicule.getBatterie() / vehicule.getBatterieMax()), 255 * (vehicule.getBatterie() / vehicule.getBatterieMax()), 255));
    text.setStyle(sf::Text::Bold);
    text.setPosition(vehicule.getPosition().x, vehicule.getPosition().y);
    this->window->draw(text);
}