//
// Created by minejona on 23/03/2021.
//

#ifndef LIFPROJET_VISUALISATIONETATVEHICULE_H
#define LIFPROJET_VISUALISATIONETATVEHICULE_H


#include "../Voiture/Voiture.h"

class VisualisationEtatVehicule {
public:
    VisualisationEtatVehicule(sf::RenderWindow* window);
    void afficherEtatBatterie(Vehicule& vehicule);

private:
    sf::RenderWindow* window;
    sf::Font font;
};


#endif //LIFPROJET_VISUALISATIONETATVEHICULE_H