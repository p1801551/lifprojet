//
// Created by minejona on 16/03/2021.
//

#ifndef LIFPROJET_VISUALISATIONCHEMIN_H
#define LIFPROJET_VISUALISATIONCHEMIN_H


#include <SFML/Graphics/RenderWindow.hpp>
#include <unordered_map>
#include "../Map/Map.h"

using namespace std;

class VisualisationChemin {
public:
    VisualisationChemin(sf::RenderWindow* window, Map& map);
    void draw(vector<sf::Vector2i>& chemin);
    ~VisualisationChemin();

private:
    Map map;
    sf::RenderWindow* window;
    unordered_map<vector<sf::Vector2i>*, int[3]> chemins;
};


#endif //LIFPROJET_VISUALISATIONCHEMIN_H
