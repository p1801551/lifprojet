//
// Created by minejona on 16/03/2021.
//

#include <SFML/Graphics/RectangleShape.hpp>
#include "VisualisationChemin.h"

VisualisationChemin::VisualisationChemin(sf::RenderWindow* window, Map& map) {
    this->map = map;
    this->window = window;
}

void VisualisationChemin::draw(vector<sf::Vector2i>& chemin) {
    if (chemin.empty() == false) {
        int i = 0;

        if (this->chemins.count(&chemin) == 0) {
            this->chemins[&chemin][0] = rand() % 256;
            this->chemins[&chemin][1] = rand() % 256;
            this->chemins[&chemin][2] = rand() % 256;
        }

        while (i < chemin.size() - 1) {
            //std::cout << "Test : " << chemin0.top() << " (" << map.GetIndice2D(chemin0.top()).x << "," << map.getIndice2D(chemin0.top()).y << ")" << std::endl;
            sf::RectangleShape rectangle(sf::Vector2f(32.0,32.0));
            rectangle.setFillColor(sf::Color(this->chemins[&chemin][0],this->chemins[&chemin][1],this->chemins[&chemin][2],100));
            rectangle.setOutlineThickness(1.0);
            rectangle.setOutlineColor(sf::Color(255,255,255,255));
            rectangle.setPosition(chemin[i].x * 32, chemin[i].y * 32);
            this->window->draw(rectangle);
            i++;
        }
        sf::CircleShape circle(12);
        circle.setFillColor(sf::Color(this->chemins[&chemin][0],this->chemins[&chemin][1],this->chemins[&chemin][2],100));
        circle.setOutlineThickness(1.0);
        circle.setOutlineColor(sf::Color(255,255,255,255));
        circle.setPosition(chemin[i].x * 32 + 4, chemin[i].y * 32 + 4);
        this->window->draw(circle);
    }
}

VisualisationChemin::~VisualisationChemin() {
    delete &this->chemins;
}