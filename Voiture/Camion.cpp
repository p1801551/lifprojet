//
// Created by kiraloup on 08/03/2021.
//

#include <iostream>
#include "Camion.h"

Camion::Camion(MoniteurChemin* moniteurChemin, sf::Vector2i& positionInitiale, Map* map, double batterieMax) : Vehicule(moniteurChemin, positionInitiale, map, batterieMax) {
    initialiserCamion(positionInitiale, map, batterieMax);
}

Camion::Camion(MoniteurChemin* moniteurChemin, sf::Vector2i& positionInitiale, Map* map, double batterieMax, bool insertionAleatoireDObjectifs) : Vehicule(moniteurChemin, positionInitiale, map, batterieMax, insertionAleatoireDObjectifs) {
    initialiserCamion(positionInitiale, map, batterieMax);
}

void Camion::initialiserCamion(sf::Vector2i& positionInitiale, Map* map, double batterieMax) {
    load("Textures/Vago.png", *(new sf::Vector2f((float)positionInitiale.x * 32.0, (float)positionInitiale.y * 32.0)));
}

bool Camion::load(const string& textureCamion, sf::Vector2f positionInitiale) {

    if(!texture.loadFromFile(textureCamion, sf::IntRect(64,0,32,32)))
    {
        std::cout << "Erreur : la texture du camion n'a pas pu être chargée" << std::endl;
        return false;
    }

    sprite.setTexture(texture);
    this->setPosition(positionInitiale);
    return true;
}

void Camion::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    states.transform *= getTransform();

    states.texture = &texture;

    target.draw(sprite, states);
}