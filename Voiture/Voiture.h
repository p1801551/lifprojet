//
// Created by kiraloup on 04/02/2021.
//

#ifndef LIFPROJET_VOITURE_H
#define LIFPROJET_VOITURE_H

#include <iostream>
#include <SFML/Graphics.hpp>
#include "../Obj/ListObjectifs.h"
#include "Vehicule.h"
#include <vector>
#include "../Obj/Objectif.h"
#include "../Bat/StationRecharge.h"

using namespace std;

class Voiture : public Vehicule {
public:
    Voiture(MoniteurChemin* moniteurChemin, sf::Vector2i& positionInitiale, Map* map, double batterieMax);
    Voiture(MoniteurChemin* moniteurChemin, sf::Vector2i& positionInitiale, Map* map, double batterieMax, bool insertionAleatoireDObjectifs);

    // Fonctions liées à SFML
    bool load(const string& textureVoiture, sf::Vector2f positionInitiale);
    virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const;

private:
    void initialiserVoiture(sf::Vector2i& positionInitiale, Map* map, double batterieMax);
};

#endif //LIFPROJET_VOITURE_H