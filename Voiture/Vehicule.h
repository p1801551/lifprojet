//
// Created by nico on 08/03/2021.
//

#ifndef LIFPROJET_VEHICULE_H
#define LIFPROJET_VEHICULE_H

#include <SFML/Graphics.hpp>
#include "../Map/Map.h"
#include "../Obj/ListObjectifs.h"
#include "../Bat/StationRecharge.h"
#include "../Moniteur/MoniteurChemin.h"

class Vehicule: public sf::Drawable, public sf::Transformable {
public:
    enum class Direction { Haut, Bas, Gauche, Droite };

    Vehicule(MoniteurChemin* moniteurChemin, sf::Vector2i& positionInitiale, Map* map, double batterieMax);
    Vehicule(MoniteurChemin* moniteurChemin, sf::Vector2i& positionInitiale, Map* map, double batterieMax, bool insertionAleatoireDObjectifs);

    inline int getId() const { return this->id; };
    inline float getVitesse() const { return this->vitesse; };
    inline double getPoids() const { return this->poids; };
    inline double getBatterie() const { return this->batterie; };
    inline double getBatterieMax() const { return this->batterieMax; };
    inline void setBatterie(double newBatterie) { this->batterie = newBatterie; };
    inline bool consommerBatterie(double batterieAConsommer) { if (this->batterie - batterieAConsommer >= 0) { this->batterie -= batterieAConsommer; return true; } else { return false; } };
    inline bool batteriePeutEtreConsommee(double batterie) { if (this->batterie - batterie >= 0) return true; else return false; };
    inline void ajouterBatterie(double batterieSupplementaire) { if (this->batterie + batterieSupplementaire <= this->batterieMax) { this->batterie += batterieSupplementaire; } };
    inline void remplirBatterieABloc() { this->batterie = this->batterieMax; };
    inline bool batteriePleine() { return (this->batterie == this->batterieMax); };
    inline bool batterieVide() { return (this->batterie == 0.0); };

    inline ListObjectifs* getObjectifs() { return this->objectifs; };
    inline bool objectifsTermines() { return (this->objectifs->estVide()); };
    inline vector<sf::Vector2i>& getCurrentChemin() { return this->currentChemin; };
    void followPath(std::vector<sf::Vector2i>& chemin);
    inline MoniteurChemin* getMoniteurChemin() { return this->moniteurChemin; };

    void demarrer();

    inline sf::Vector2i getCurrentPosition() { return this->currentPosition; };

    bool haut();
    bool bas();
    bool gauche();
    bool droite();
    void updateDirection(int offsetX, int offsetY);
    bool deplacerVehicule(int offsetX, int offsetY);

    inline Vehicule::Direction getDirection() { return this->direction; };

    void addPoids(Objectif);
    void removePoids(Objectif obj);
    float negociation(StationRecharge& st);

    void trouverBorneLaPlusRentable();

    bool insertionAleatoireDObjectifs = false;
    bool seDirigeVersUneBorne = false;

    ~Vehicule();

protected:
    void initialiserVehicule(MoniteurChemin* moniteurChemin, sf::Vector2i& positionInitiale, Map* map, double batterieMax);
    /* Pour calculer la consomation d'energy, on a besoin de calculer le chargement sur la route
     * Ftot = Fi + Fs + Fr + Fa
     *
     * Pour cela on a besoin :
     * Fi [N] – inertial force
     * Fs [N] – road slope force
     * Fr [N] – road load force
     * Fa [N] – aerodynamic drag force
     *
     * Fi = mv (mass total du véhicule) * av = accélération du véhicule
     * La pente du chemin
     * Fs = mv * g(la pesanteur 9,81 dans mes souvenirs) * sin(as) (l'angle de la pente)
     * Fr = mv * g * crr * cos(as)
     * Fa = 1/2 * p (densité de l'air à 20 degré) * cd(la trainé de l'air) * A (devant du véhicule) * (v(vitesse du véhicule))²
     * Ptot = Ftot * v(vitesse du véhicule)
     * Puis on fait l'intégrale de Etot
     * */
    float calculerLaConsomationEnergie();
    sf::Sprite sprite;
    sf::Texture texture;
    //caractéristique du vehicule
    static int nbVehicules;
    int id;
    float vitesse;
    double poids;
    double batterie;
    double batterieMax;
    ListObjectifs* objectifs;
    std::vector<sf::Vector2i> currentChemin;
    Vehicule::Direction direction;
    double moyenneEnergieConsommee;
    double energieConsommeeAuTotal;
    int nbRoutesParcourues;

    Map* map;
    sf::Vector2i currentPosition;

    MoniteurChemin* moniteurChemin;
};

#endif //LIFPROJET_VEHICULE_H