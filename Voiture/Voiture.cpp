//
// Created by kiraloup on 04/02/2021.
//

#include "Voiture.h"
#include <iostream>
#include "../Route/Route.h"

using namespace std;

// La même hauteur mais la longueur de la voiture est 2 fois 16

Voiture::Voiture(MoniteurChemin* moniteurChemin, sf::Vector2i& positionInitiale, Map* map, double batterieMax) : Vehicule(moniteurChemin, positionInitiale, map, batterieMax) {
    initialiserVoiture(positionInitiale, map, batterieMax);
}

Voiture::Voiture(MoniteurChemin* moniteurChemin, sf::Vector2i& positionInitiale, Map* map, double batterieMax, bool insertionAleatoireDObjectifs) : Vehicule(moniteurChemin, positionInitiale, map, batterieMax, insertionAleatoireDObjectifs) {
    initialiserVoiture(positionInitiale, map, batterieMax);
}

void Voiture::initialiserVoiture(sf::Vector2i& positionInitiale, Map* map, double batterieMax) {
    this->load("Textures/Vago.png", *(new sf::Vector2f((float)positionInitiale.x * 32.0, (float)positionInitiale.y * 32.0)));
}

bool Voiture::load(const string& textureVoiture, sf::Vector2f positionInitiale)
{
    if(!texture.loadFromFile(textureVoiture, sf::IntRect(0, 0, 32, 32)))
    {
        std::cout << "Erreur : la texture de la voiture n'a pas pu être chargée" << std::endl;
        return false;
    }

    sprite.setTexture(texture);
    this->setPosition(positionInitiale);
    return true;
}

void Voiture::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    // Apply the tileset texture
    states.texture = &texture;

    // Draw the vertex array
    target.draw(sprite, states);
}