//
// Created by kiraloup on 08/03/2021.
//

#ifndef LIFPROJET_CAMION_H
#define LIFPROJET_CAMION_H

#include <SFML/Graphics.hpp>
#include "../Obj/ListObjectifs.h"
#include "Vehicule.h"

using namespace std;

class Camion : public Vehicule {
public:
    Camion(MoniteurChemin* moniteurChemin, sf::Vector2i& positionInitiale, Map* map, double batterieMax);
    Camion(MoniteurChemin* moniteurChemin, sf::Vector2i& positionInitiale, Map* map, double batterieMax, bool insertionAleatoireDObjectifs);

    bool load(const string& textureCamion, sf::Vector2f positionInitiale);
    virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const;

private:
    void initialiserCamion(sf::Vector2i& positionInitiale, Map* map, double batterieMax);
};

#endif //LIFPROJET_CAMION_H