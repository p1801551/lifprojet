//
// Created by nico on 08/03/2021.
//

#include <unistd.h>
#include "Vehicule.h"
#include "../Route/Route.h"
#include "../Divers/Tools.h"
#include <thread>
#include <cmath>

int Vehicule::nbVehicules = 0;

Vehicule::Vehicule(MoniteurChemin* moniteurChemin, sf::Vector2i& positionInitiale, Map* map, double batterieMax) {
    initialiserVehicule(moniteurChemin, positionInitiale, map, batterieMax);
}

Vehicule::Vehicule(MoniteurChemin* moniteurChemin, sf::Vector2i& positionInitiale, Map* map, double batterieMax, bool insertionAleatoireDObjectifs) {
    initialiserVehicule(moniteurChemin, positionInitiale, map, batterieMax);
    this->insertionAleatoireDObjectifs = insertionAleatoireDObjectifs;
}

void Vehicule::initialiserVehicule(MoniteurChemin* moniteurChemin, sf::Vector2i& positionInitiale, Map* map, double batterieMax) {
    this->id = Vehicule::nbVehicules;
    Vehicule::nbVehicules++;
    this->map = map;
    this->moniteurChemin = moniteurChemin;
    this->currentPosition = positionInitiale;
    this->map->getMap()[this->currentPosition.x][this->currentPosition.y]->lockModifications();
    this->map->getMap()[this->currentPosition.x][this->currentPosition.y]->setPractical(false);
    this->map->getMap()[this->currentPosition.x][this->currentPosition.y]->unlockModifications();

    this->vitesse = 32.0f;
    this->batterieMax = batterieMax;
    this->batterie = batterieMax;
    this->moyenneEnergieConsommee = 0;
    this->energieConsommeeAuTotal = 0;
    this->nbRoutesParcourues = 0;

    this->poids = (rand() % 700 + 300);
    this->objectifs = new ListObjectifs(this->map);
}

void Vehicule::demarrer() {
    if (this->objectifsTermines() == false && this->currentChemin.empty() == true) {
        this->moniteurChemin->calculerChemin(this->id, this->getCurrentPosition(), this->objectifs->getPremierObjectif());
        this->currentChemin = this->moniteurChemin->getChemin(this->id);
        while (this->currentChemin.empty() == false) {
            followPath(this->currentChemin);
        }
    }
    ((Route*)this->map->getMap()[currentPosition.x][currentPosition.y])->lockModifications();
    ((Route*)this->map->getMap()[currentPosition.x][currentPosition.y])->setVehiculePresent(nullptr);
    ((Route*)this->map->getMap()[currentPosition.x][currentPosition.y])->unlockModifications();
}

bool Vehicule::haut() {
    sf::Vector2i newPosition = this->map->north(this->currentPosition);
    return deplacerVehicule(newPosition.x - this->currentPosition.x, newPosition.y - this->currentPosition.y);
}

bool Vehicule::bas() {
    sf::Vector2i newPosition = this->map->south(this->currentPosition);
    return deplacerVehicule(newPosition.x - this->currentPosition.x, newPosition.y - this->currentPosition.y);
}

bool Vehicule::gauche() {
    sf::Vector2i newPosition = this->map->west(this->currentPosition);
    return deplacerVehicule(newPosition.x - this->currentPosition.x, newPosition.y - this->currentPosition.y);
}

bool Vehicule::droite() {
    sf::Vector2i newPosition = this->map->east(this->currentPosition);
    return deplacerVehicule(newPosition.x - this->currentPosition.x, newPosition.y - this->currentPosition.y);
}

bool Vehicule::deplacerVehicule(int offsetX, int offsetY) {
    sf::Vector2i newPosition = *(new sf::Vector2i(this->currentPosition.x + offsetX, this->currentPosition.y + offsetY));
    if (offsetX >= 0 || offsetY >= 0) {
        double consommationEnergie = calculerLaConsomationEnergie();
        double batterieAConsommer = ((Route*)(this->map->getMap()[newPosition.x][newPosition.y]))->getConsommationElectrique() + consommationEnergie;

        this->map->getMap()[currentPosition.x][currentPosition.y]->lockWith(this->map->getMap()[newPosition.x][newPosition.y]);

        if (this->map->getMap()[newPosition.x][newPosition.y]->isPractical() == true && this->batteriePeutEtreConsommee(batterieAConsommer)) {
            updateDirection(offsetX, offsetY);
            if (((Route*)this->map->getMap()[newPosition.x][newPosition.y])->getVehiculePresent() == nullptr || Tools::directionInverse(((Route*)this->map->getMap()[newPosition.x][newPosition.y])->getVehiculePresent()->getDirection()) == this->direction) {
                this->consommerBatterie(batterieAConsommer);

                ((Route*)this->map->getMap()[currentPosition.x][currentPosition.y])->setVehiculePresent(nullptr);
                ((Route*)this->map->getMap()[newPosition.x][newPosition.y])->setVehiculePresent(this);

                this->map->getMap()[currentPosition.x][currentPosition.y]->unlockWith(this->map->getMap()[newPosition.x][newPosition.y]);

                this->move(offsetX * 32, offsetY * 32);
                this->currentPosition = newPosition;

                this->nbRoutesParcourues++;
                this->energieConsommeeAuTotal += consommationEnergie;
                this->moyenneEnergieConsommee = this->energieConsommeeAuTotal / nbRoutesParcourues;

                // On cherche s'il y a des bornes de recharge à proximité
                queue<sf::Vector2i> bornesVoisines = this->map->getBornesVoisines(this->currentPosition);
                while (bornesVoisines.empty() == false) {
                    if (this->map->getMap()[bornesVoisines.front().x][bornesVoisines.front().y]->getTypeTile() == Tile::TypeTile::StationRecharge) {
                        remplirBatterieABloc();
                    }
                    bornesVoisines.pop();
                }

                // On vérifie si ce nouveau déplacement correspond au chemin de l'objectif. Si c'est le cas, on dépile le chemin. Si le chemin est vide, on appelle l'objectif suivant avec objectifSuivant()
                if (this->currentChemin.empty() == false && this->currentChemin[0].x == this->currentPosition.x && this->currentChemin[0].y == this->currentPosition.y) {
                    this->currentChemin.erase(this->currentChemin.begin());
                    if (this->batterie / this->batterieMax < 0.45 && seDirigeVersUneBorne == false) {
                        trouverBorneLaPlusRentable();
                    }
                    else if (this->currentChemin.empty() == true) {
                        this->removePoids(this->objectifs->getPremierObjectif());
                        if (this->objectifs->objectifSuivant() == true) {
                            this->addPoids(this->objectifs->getPremierObjectif());
                            this->moniteurChemin->calculerChemin(this->id, this->getCurrentPosition(), this->objectifs->getPremierObjectif());
                            this->currentChemin = this->moniteurChemin->getChemin(this->id);
                            this->seDirigeVersUneBorne = false;
                        }
                    }
                }
                return true;
            }
        }
        this->map->getMap()[currentPosition.x][currentPosition.y]->unlockWith(this->map->getMap()[newPosition.x][newPosition.y]);
    }

    return false;
}

void Vehicule::trouverBorneLaPlusRentable() {
    // On obtient une liste des bornes ordonnées de la plus proche à la plus lointaine
    vector<sf::Vector2i> bornesLesPlusProches = this->map->getBornesAtteignablesLesPlusProches(currentPosition, batterie, moyenneEnergieConsommee);
    sf::Vector2i borneLaPlusRentable;
    int ind = 0;
    float prixMin = -1;
    float currentPrix;

    // On recherche la borne la plus proche possible pour laquelle la négociation réussit
    while (prixMin == -1) {
        prixMin = negociation(*((StationRecharge*)this->map->getMap()[bornesLesPlusProches[ind].x][bornesLesPlusProches[ind].y - 1])) != -1;
        ind++;
    }

    // On recherche d'autres bornes plus lointaines pour voir si leur prix est plus attractif
    borneLaPlusRentable = bornesLesPlusProches[ind - 1];
    while (ind < bornesLesPlusProches.size()) {
        currentPrix = negociation(*((StationRecharge*)this->map->getMap()[bornesLesPlusProches[ind].x][bornesLesPlusProches[ind].y - 1]));
        if (currentPrix != -1 && currentPrix < prixMin) {
            prixMin = currentPrix;
            borneLaPlusRentable = bornesLesPlusProches[ind];
        }
        ind++;
    }

    // On insère la borne de recharge électrique la plus rentable en coût et en distance en tant qu'objectif prioritaire
    this->objectifs->insererObjectifPrioritaire(*(new Objectif(borneLaPlusRentable.x, borneLaPlusRentable.y)));
    this->moniteurChemin->calculerChemin(this->id, this->getCurrentPosition(), this->objectifs->getPremierObjectif());
    this->currentChemin = this->moniteurChemin->getChemin(this->id);
    this->seDirigeVersUneBorne = true;
}

void Vehicule::updateDirection(int offsetX, int offsetY) {
    if (offsetX == 0 && offsetY == -1) {
        this->direction = Direction::Haut;
    }
    else if (offsetX == 0 && offsetY == 1) {
        this->direction = Direction::Bas;
    }
    else if (offsetX == -1 && offsetY == 0) {
        this->direction = Direction::Gauche;
    }
    else if (offsetX == 1 && offsetY == 0) {
        this->direction = Direction::Droite;
    }
}

void Vehicule::followPath(std::vector<sf::Vector2i>& chemin) {
    if (chemin.empty() == false) {
        sf::Vector2i currentPlayerPosition = this->getCurrentPosition();
        sf::Vector2i nextPlayerPosition = this->getCurrentChemin()[0];

        int offsetX = nextPlayerPosition.x - currentPlayerPosition.x;
        int offsetY = nextPlayerPosition.y - currentPlayerPosition.y;

        if (offsetX <= 1 && offsetX >= -1 && offsetY <= 1 && offsetY >= -1) {
            this->deplacerVehicule(offsetX, offsetY);
        }

        usleep(100000);
    }
}

void Vehicule::addPoids(Objectif obj) {
    this->poids += obj.getPoidsMarchandise();
}

void Vehicule::removePoids(Objectif obj) {
    this->poids -= obj.getPoidsMarchandise();
}
//mettre if meteo = soleil on va faire accepter direct sinon on négo
float Vehicule::negociation(StationRecharge& st) {
    float p = st.getPrix();
    cout << "p : " << p << endl;
    float r = p / 2;
    float res = 0;
    float a = p;
    res = st.propose(r, a,map->getMeteo());
    for (int i = 0; i < 3; i++) {
        cout << "tour : " << i << " r = " << r << " a = " << a << " res = " << res << " p = " << p << endl;
        if (res == r) {
            cout << "res == r" << endl;
            return res;
        }
        a = a / 2;
        r = r + a;
        res = st.propose(r, a,map->getMeteo());
    }
    return -1;
}

float Vehicule::calculerLaConsomationEnergie() {
    float masse = this->poids;
    //normalement, il faut calculer v = delta x / delta t = 100 m / 5s = 20m/s
    float velociteVoiture = 20.0;
    // a = 2 * (Δd - v_i * Δt) / Δt² = 2 * (100 - 0 * 5) / 5²
    float acceleration = 10.0;

    float forceInitial = masse * velociteVoiture * acceleration;
    //std::cout << " la force intial est de " << forceInitial << std::endl;

    //drag force
    // traine = 1/2 * la densité de l'air (10 degré) * resistance de l'air *  coefficient de resistance de l'air * A
    //The average modern automobile achieves a drag coefficient of between 0.25 and 0.3. (source internet)
    float traine = 1/2 * 1.24  * 0.25 * 2.0 *  std::pow(velociteVoiture, 3);

    // e3 = masse * force de gravité  * différence d'élévation entre 1 m
    float e3 = masse * 9.8 * Tools::generateRandomNumber(0,1);

    // e4 = rolling resistance * masse  * force de gravité * vélocité de la voiture
    //The coefficient of rolling resistance for a pneumatic tire used in motor vehicles on a smooth asphalt
    // or concrete surface is indicated in the range 0.007 to 0.02 [1-9], most often indicated around 0.012.z
    float e4 = 0.2 * masse * 9.81 * velociteVoiture;

    float consomationElec = forceInitial + traine + e3 + e4;

    //std::cout << " la consomation élétrique " << consomationElec/1000 << std::endl;

    return consomationElec/1000;
}

Vehicule::~Vehicule() {
    /*std::cout << " Dest V" << std::endl;
    this->id = -1;
    this->map->getMap()[currentPosition.x][currentPosition.y]->lockModifications();
    ((Route*)this->map->getMap()[currentPosition.x][currentPosition.y])->setVehiculePresent(nullptr);
    this->map->getMap()[currentPosition.x][currentPosition.y]->unlockModifications();
    delete this->objectifs;
    //this->currentChemin.clear();
    //this->currentChemin.(this->currentChemin.begin(), this->currentChemin.end());
    auto it = currentChemin.begin();
    while (it != currentChemin.end()) {
        it = currentChemin.erase(it);
    }*/
}
