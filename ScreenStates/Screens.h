//
// Created by kiraloup on 01/03/2021.
//

#ifndef LIFPROJET_SCREENS_H
#define LIFPROJET_SCREENS_H
#include <SFML/Graphics.hpp>

class Screens {
public:
    virtual int Run (sf::RenderWindow& app) = 0;
};


#endif //LIFPROJET_SCREENS_H
