//
// Created by kiraloup on 01/03/2021.
//

#ifndef LIFPROJET_WEATHERSCREEN_H
#define LIFPROJET_WEATHERSCREEN_H
#include "Screens.h"
#include <SFML/Graphics.hpp>

class WeatherScreen: public Screens {

public:
    WeatherScreen();
    virtual int Run(sf::RenderWindow& app);

private:
    bool checkMeteo(sf::Vector2i&,sf::Text&);
    bool playing;
};


#endif //LIFPROJET_WEATHERSCREEN_H
