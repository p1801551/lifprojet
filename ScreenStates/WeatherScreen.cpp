//
// Created by kiraloup on 01/03/2021.
//

#include <iostream>
#include "WeatherScreen.h"



int WeatherScreen::Run(sf::RenderWindow &app) {

    sf::Event event;
    sf::Font font;
    playing = true;

    if (!font.loadFromFile("Textures/OpenSans-Regular.ttf"))
    {
        std::cerr << " erreur de chargment du fichier " << std::endl;
        return -1;
    }

    //on charge la police de caractère depuis le disque
    sf::Text text1;
    text1.setFont(font);
    text1.setString("Soleil");
    text1.setCharacterSize(34);
    text1.setPosition(80.0,0.0f);
    sf::Text text2;
    text2.setFont(font);
    text2.setString("Neige");
    text2.setCharacterSize(40);
    text2.setPosition(text1.getPosition().x,text1.getPosition().y +64 );
    sf::Text text3;
    text3.setFont(font);
    text3.setString("Pluie");
    text3.setCharacterSize(40);
    text3.setPosition(text1.getPosition().x, text1.getPosition().y + 128 );


    while (playing)
    {
        while (app.pollEvent(event)) {
            // Window closed
            if (event.type == sf::Event::Closed) {
                return (-1);
            }

            if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                // renvoie les coordonnées de la souris dans la fenetre
                sf::Vector2i position = sf::Mouse::getPosition(app);
                if (checkMeteo(position, text1))
                {
                    std::cout << "click sur la zone de texte soleil " << std::endl;
                    return 1;
                }
                else if(checkMeteo(position, text2))
                {
                    std::cout << "click sur la zone de texte neige " << std::endl;
                    return 2;
                }
                else if(checkMeteo(position, text3))
                {
                    std::cout << "click sur la zone de texte Pluie " << std::endl;
                    return 3;
                }
                else
                {
                    std::cout << "click sur en dehors des zone de texte" << std::endl;
                }

            }
        }

        app.clear(sf::Color(135,206,235));
        app.draw(text1);
        app.draw(text2);
        app.draw(text3);
        app.display();
    }
    return 1;
}

WeatherScreen::WeatherScreen() {
    playing = false;

}

bool WeatherScreen::checkMeteo(sf::Vector2i &position, sf::Text &textbox) {
    sf::Vector2f positionbox = textbox.getPosition();
    if( (position.x >=  positionbox.x && position.x <= (positionbox.x + textbox.getGlobalBounds().width))
    && (position.y >= positionbox.y && position.y <= positionbox.y + textbox.getGlobalBounds().height))
    {
        return true;
    }
    return false;
}
