//
// Created by minejona on 23/02/2021.
//

#ifndef LIFPROJET_PATHFINDER_H
#define LIFPROJET_PATHFINDER_H

#include <tuple>
#include <vector>
#include <queue>
#include <stack>
#include "../Map/Map.h"
#include "PathFindingTile.h"
#include "../Obj/Objectif.h"

using namespace std;

class PathFinder {
public:
    PathFinder(Map map);
    vector<sf::Vector2i> trouverChemin(sf::Vector2i positionDeDepart, Objectif& objectif);
    vector<vector<PathFindingTile*>> getPathFindingMap();
    ~PathFinder();

private:
    sf::Vector2i getTileAExplorerDePlusCourtChemin();
    double BatterieVoitureMax;
    Map map;
    vector<vector<PathFindingTile*>> pathFindingMap;
    vector<vector<double>> plusCourtsChemins;
    vector<vector<sf::Vector2i>> cheminLePlusProche;
    vector<sf::Vector2i> tilesAExplorer;
};

#endif //LIFPROJET_PATHFINDER_H