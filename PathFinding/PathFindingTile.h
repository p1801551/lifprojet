//Floyd–Warshall
// Created by minejona on 23/02/2021.
//

#ifndef LIFPROJET_PATHFINDINGTILE_H
#define LIFPROJET_PATHFINDINGTILE_H

#include "../Map/Tile.h"

class PathFindingTile : public Tile {
public:
    PathFindingTile(Tile::TypeTile typeTile);
    int Statut; // 0 = Non explorée, 1 = A explorer, 2 = Explorée
};

#endif //LIFPROJET_PATHFINDINGTILE_H