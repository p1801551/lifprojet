//
// Created by minejona on 23/02/2021.
//

#include "PathFinder.h"
#include "PathFindingTile.h"
#include "../Route/Route.h"
#include <vector>
#include <queue>
#include <stack>

using namespace std;

PathFinder::PathFinder(Map map) {
    this->BatterieVoitureMax = 100.0;

    this->map = map;
    vector<vector<PathFindingTile*>> pathfindingmap = *(new vector<vector<PathFindingTile*>>());
    vector<vector<double>> pluscourtschemins = *(new vector<vector<double>>());
    vector<vector<sf::Vector2i>> originelaplusproche = *(new vector<vector<sf::Vector2i>>());

    this->pathFindingMap = pathfindingmap;
    this->plusCourtsChemins = pluscourtschemins;
    this->cheminLePlusProche = originelaplusproche;

    for (int x = 0; x < this->map.getLargeur(); x++) {
        this->pathFindingMap.push_back(*(new vector<PathFindingTile*>()));
        this->plusCourtsChemins.push_back(*(new vector<double>()));
        this->cheminLePlusProche.push_back(*(new vector<sf::Vector2i>()));

        for (int y = 0; y < this->map.getLongueur(); y++) {
            this->pathFindingMap[x].push_back(new PathFindingTile(map.getMap()[x][y]->getTypeTile()));
            this->plusCourtsChemins[x].push_back(-1);
            this->cheminLePlusProche[x].push_back(*(new sf::Vector2i(-1,-1)));
        }
    }
}

vector<sf::Vector2i> PathFinder::trouverChemin(sf::Vector2i positionDeDepart, Objectif& objectif) {
    vector<sf::Vector2i> cheminTrouve;
    sf::Vector2i coordsDepart = positionDeDepart;
    sf::Vector2i coordsArrivee = objectif.getPosition();

    if (coordsDepart != coordsArrivee) {
        for (int x = 0; x < this->map.getLargeur(); x++)
        {
            for (int y = 0; y < this->map.getLongueur(); y++) {
                this->pathFindingMap[x][y]->Statut = 0;
                this->plusCourtsChemins[x][y] = -1;
                this->cheminLePlusProche[x][y] = *(new sf::Vector2i(-1,-1));
            }
        }

        sf::Vector2i currentCoords = coordsDepart;
        queue<sf::Vector2i> routesVoisines;
        double valuation;
        bool voitureRechargee = false;

        this->plusCourtsChemins[currentCoords.x][currentCoords.y] = ((Route*)(this->map.getMap()[currentCoords.x][currentCoords.y]))->getConsommationElectrique();
        this->cheminLePlusProche[currentCoords.x][currentCoords.y] = currentCoords;

        while (currentCoords.x != -1) {
            routesVoisines = this->map.getCheminsVoisins(currentCoords);
            while (routesVoisines.empty() == false) {
                if (this->pathFindingMap[routesVoisines.front().x][routesVoisines.front().y]->Statut < 2) {
                    if (map.getMap()[routesVoisines.front().x][routesVoisines.front().y]->getTypeTile() == Tile::TypeTile::StationRecharge) {
                        this->pathFindingMap[routesVoisines.front().x][routesVoisines.front().y]->Statut = 2;
                        voitureRechargee = true;
                    }
                    else {
                        this->pathFindingMap[routesVoisines.front().x][routesVoisines.front().y]->Statut = 1;
                    }
                    if (voitureRechargee) {
                        valuation = 0.0;
                    }
                    else {
                        valuation = this->plusCourtsChemins[currentCoords.x][currentCoords.y] + ((Route*)(this->map.getMap()[routesVoisines.front().x][routesVoisines.front().y]))->getConsommationElectrique();
                    }
                    this->tilesAExplorer.push_back(routesVoisines.front());
                    if (valuation < this->plusCourtsChemins[routesVoisines.front().x][routesVoisines.front().y] || this->plusCourtsChemins[routesVoisines.front().x][routesVoisines.front().y] == -1) {
                        this->plusCourtsChemins[routesVoisines.front().x][routesVoisines.front().y] = valuation;
                        this->cheminLePlusProche[routesVoisines.front().x][routesVoisines.front().y] = currentCoords;
                    }
                }
                routesVoisines.pop();
            }
            voitureRechargee = false;
            this->pathFindingMap[currentCoords.x][currentCoords.y]->Statut = 2;
            currentCoords = getTileAExplorerDePlusCourtChemin();
        }

        cheminTrouve.push_back(coordsArrivee);
        sf::Vector2i currentPosition = this->cheminLePlusProche[coordsArrivee.x][coordsArrivee.y];
        while (currentPosition != coordsDepart) {
            cheminTrouve.push_back(currentPosition);
            if (currentPosition.x >= this->map.getLargeur() || currentPosition.y >= this->map.getLongueur() || currentPosition.x < 0 || currentPosition.y < 0) {
                cheminTrouve.empty();
                return cheminTrouve;
            }
            currentPosition = this->cheminLePlusProche[currentPosition.x][currentPosition.y];
        }
        reverse(cheminTrouve.begin(), cheminTrouve.end());
    }

    return cheminTrouve;
}

sf::Vector2i PathFinder::getTileAExplorerDePlusCourtChemin() {
    if (this->tilesAExplorer.empty() == false) {
        int ind = 0;
        sf::Vector2i indicePlusCourtChemin = this->tilesAExplorer[0];
        int plusCourtChemin = ((Route*)(this->map.getMap()[this->tilesAExplorer[0].x][this->tilesAExplorer[0].y]))->getConsommationElectrique();
        for (int i = 0; i < this->tilesAExplorer.size(); i++) {
            if (((Route*)(this->map.getMap()[this->tilesAExplorer[i].x][this->tilesAExplorer[i].y]))->getConsommationElectrique() < plusCourtChemin) {
                ind = i;
                indicePlusCourtChemin = this->tilesAExplorer[i];
                plusCourtChemin = ((Route*)(this->map.getMap()[this->tilesAExplorer[i].x][this->tilesAExplorer[i].y]))->getConsommationElectrique();
            }
        }
        this->tilesAExplorer.erase(this->tilesAExplorer.begin() + ind);
        return indicePlusCourtChemin;
    }
    else {
        return *(new sf::Vector2i(-1,-1));
    }
}

vector<vector<PathFindingTile*>> PathFinder::getPathFindingMap() {
    return this->pathFindingMap;
}

PathFinder::~PathFinder() {
    //delete &this->pathFindingMap;
    //delete &this->cheminLePlusProche;
    //delete &this->tilesAExplorer;
    //delete &this->plusCourtsChemins;
}