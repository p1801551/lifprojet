//
// Created by nico on 08/02/2021.
//

#ifndef LIFPROJET_STATIONRECHARGE_H
#define LIFPROJET_STATIONRECHARGE_H
#include <iostream>
#include <SFML/Graphics.hpp>
#include <mutex>
#include <thread>
#include "../Map/Tile.h"
#include "../Map/Map.h"

using  namespace std;

class StationRecharge : public Tile {
public:
    StationRecharge();
    std::mutex m;
    inline float getPrix() { unique_lock<std::mutex>l(m);return this->prix; };
    inline void setPrix(float p) { unique_lock<std::mutex>l(m);this->prix=p; };
    float propose(float p, float&,int);


private:

    int nombreDePlacesTotal;
    int nombreDePlacesLibres;
    
    // La vitesse de rechargement est en kiloWatt
    float vitesseDeRechargement;
    float prix;
};


#endif //LIFPROJET_STATIONRECHARGE_H
