//total
// Created by nico on 08/02/2021.
//

#include "StationRecharge.h"
#include "../Divers/Tools.h"

StationRecharge::StationRecharge() : Tile(TypeTile::StationRecharge) {
    this->practical = false;
    this->vitesseDeRechargement = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/10));

    // Un nombre qui peut aller de 10 jusqu'à 50
    this->nombreDePlacesTotal = rand() % 50 + 10;

    // Un nombre qui peut aller de 0 jusqu'au nombre de places total
    this->nombreDePlacesLibres = rand() % nombreDePlacesTotal + 0;

    this->prix=(float(rand())/float((RAND_MAX)) * 1.0);
}

float StationRecharge::propose(float r,float& a,int meteo){
    if ( meteo== 1) {
        return r;
    }else if (meteo == 2 ||meteo==3){
        int i = Tools::pileOuFace();
        std::cout << "pouf :"<<i<<std::endl;
        if( i== true){
            std::cout << "On accepte :"<<i<<std::endl;
            return r;
        }
        else{
            std::cout << "On réachérie :"<<i<<std::endl;
            a=a/2;// on divise a par 2 de chaque coté pour le vendeur et l'acheteur a chaque fois et pour le vendeur ou ajoute a (on veut plus cher) et pour l'acheteur on  soustrait a ( on veut moins cher)
            return (r+a);
        }
    }
    return r;
}

