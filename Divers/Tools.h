//
// Created by minejona on 22/03/2021.
//

#ifndef LIFPROJET_TOOLS_H
#define LIFPROJET_TOOLS_H

#include "../Voiture/Vehicule.h"

class Tools {
public:
    static bool pileOuFace();
    static Vehicule::Direction directionInverse(Vehicule::Direction direction);
    static double generateRandomNumber(int min, int max);
};


#endif //LIFPROJET_TOOLS_H
