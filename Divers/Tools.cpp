//
// Created by minejona on 22/03/2021.
//

#include <SFML/Graphics.hpp>
#include <random>
#include <chrono>
#include "Tools.h"

bool Tools::pileOuFace() {
    int x = std::rand() % 2;
    if (x == 1) {
        return true;
    }
    return false;
}

Vehicule::Direction Tools::directionInverse(Vehicule::Direction direction) {
    if (direction == Vehicule::Direction::Haut) {
        return Vehicule::Direction::Bas;
    }
    else if (direction == Vehicule::Direction::Bas) {
        return Vehicule::Direction::Haut;
    }
    else if (direction == Vehicule::Direction::Gauche) {
        return Vehicule::Direction::Droite;
    }
    else if (direction == Vehicule::Direction::Droite) {
        return Vehicule::Direction::Gauche;
    }
}

double Tools::generateRandomNumber(int min, int max) {
    std::mt19937_64 rng;
    // initialize the random number generator with time-dependent seed
    uint64_t timeSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::seed_seq ss{uint32_t(timeSeed & 0xffffffff), uint32_t(timeSeed>>32)};
    rng.seed(ss);
    // initialize a uniform distribution between 0 and 1
    std::uniform_real_distribution<double> unif(min, max);

    return unif(rng);
}